This issue template is designed to capture your request for an announcement to the GitLab field (Sales & CS) team. Title this issue with `Field Announcement Request: [Topic]`.

## Background 
<!-- Provide details about the announcement that you are interested in making to the field organization, including context on urgency and impact to the field, and expected outcomes/calls-to-action. Include as much detail as possible. -->
[Answer here]

### Request Info
+ Target announcement date: 
+ Impacted field teams (sales, CS, etc.): 
+ DRI(s): 
+ Recommended spokesperson: 
+ Related issue(s) and/or Handbook page(s): 

### Draft Copy
<!-- If you have an idea of what you'd like the announcement to say (i.e. a draft Slack message) please input it here. Not required, but helpful for the Field Comms team if you already have something in mind. -->
[Answer here]

## Field Communications Playbook 
<!-- Select a recommended approach after consulting the Field Communications Playbook handbook page: https://about.gitlab.com/handbook/sales/field-communications/#field-communications-playbook. Delete all others. -->

**Update Tier**
- [ ]  Tier 1
- [ ]  Tier 2
- [ ]  Tier 3

**Communication Channels**
- [ ]  #sales general channel
- [ ]  #customer-success general channel
- [ ]  #field-fyi announcement channel
- [ ]  #sales-managers channel
- [ ]  [Sales Handbook](https://about.gitlab.com/handbook/sales/)
- [ ]  [Field Enablement Webinar](https://about.gitlab.com/handbook/sales/training/sales-enablement-sessions/)
- [ ]  [CS Skills Exchange Webinar](https://about.gitlab.com/handbook/sales/training/customer-success-skills-exchange/)
- [ ]  [Field Flash newsletter](https://about.gitlab.com/handbook/sales/field-communications/field-flash-newsletter/)
- [ ]  Segment Updates for field leaders
- [ ]  [WW Field Sales Call](/handbook/sales/#bi-weekly-monday-sales-call)
- [ ]  GTM Update

### Please confirm that you have done the following prior to submitting this issue request: 
- [ ]  Consulted the [Field Communications Playbook](https://about.gitlab.com/handbook/sales/field-communications/#field-communications-playbook)
- [ ]  Considered the qualifying questions in the Playbook and provided a recommendation on Tier and Communication Channels accordingly. 
- [ ]  Provided detailed background and reference materials about the requested announcement. 

After submitting this request you'll be able to track your request on the [Field Announcements Request Board.](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/boards/1746168?label_name[]=Field%20Announcement)


----
*Do not edit below
/label ~"Field Announcement" ~"Field Announcement::Triage" ~"Field Communications" ~"field enablement"
/assign @monicaj
